<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Event;
use App\EventSpec;
use App\User;

$factory->define(User::class, function (Faker\Generator $faker)
{
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(EventSpec::class, function (Faker\Generator $faker)
{
    $channels = array_keys(config('notificator.transports'));

    return [
        'name' => $faker->slug(3),
        'params' => $faker->words(mt_rand(0, 3)),
        'channels' => $channels
    ];
});

$factory->define(Event::class, function (Faker\Generator $faker)
{
    return [
        'customer' => [
            'id' => $faker->randomNumber(),
            'email' => $faker->email,
            'phone' => $faker->phoneNumber
        ]
    ];
});
