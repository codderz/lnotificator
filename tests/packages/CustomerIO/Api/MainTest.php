<?php

use Packages\CustomerIO\Api\Client;
use Packages\CustomerIO\Api\HttpClientInterface;

class MainTest extends TestCase
{
    public function testCustomerEvent()
    {
        $fixture = [
            'id' => 'test-id',
            'name' => 'test-event-name',
            'data' => [
                'paramA' => 1,
                'paramB' => 2
            ]
        ];

        $client = $this->makeClient();

        $client
            ->httpClient()
            ->expects($this->once())
            ->method('call')
            ->with(
                $this->equalTo('POST'),
                $this->equalTo($client->endpointUrl('api', "/customers/{$fixture['id']}/events")),
                $this->equalTo([
                    'name' => $fixture['name'],
                    'data' => $fixture['data']
                ])
            );

        call_user_func_array([$client, 'customerEvent'], $fixture);
    }

    public function testUpdateCustomer()
    {
        $fixture = [
            'id' => 'test-id',
            'email' => 'test-email@codderz.com',
            'createdAt' => time(),
            'attributes' => [
                'paramA' => 1,
                'paramB' => 2
            ]
        ];

        $client = $this->makeClient();

        $client
            ->httpClient()
            ->expects($this->once())
            ->method('call')
            ->with(
                $this->equalTo('PUT'),
                $this->equalTo($client->endpointUrl('api', "/customers/{$fixture['id']}")),
                $this->equalTo([
                    'email' => $fixture['email'],
                    'created_at' => $fixture['createdAt'],
                    'attributes' => $fixture['attributes']
                ])
            );

        call_user_func_array([$client, 'updateCustomer'], $fixture);
    }

    private function makeClient()
    {
        $mockedHTTPClient = $this
            ->getMock(HttpClientInterface::class);

        return new Client($mockedHTTPClient);
    }
}
