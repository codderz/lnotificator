<?php

return [

    'transports' => [
        'email' => \App\Services\Notificator\Transports\Email\CustomerIOTransport::class,
        'sms' => \App\Services\Notificator\Transports\Sms\FakeSmsTransport::class
    ]

];
