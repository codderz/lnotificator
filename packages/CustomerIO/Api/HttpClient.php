<?php

namespace Packages\CustomerIO\Api;

class HttpClient implements HttpClientInterface
{
    private
        $siteId,
        $apiKey,
        $debug;

    public function endpoint()
    {
        return $this->endpoint;
    }

    public function __construct($siteId, $apiKey, $debug = false)
    {
        $this->siteId = $siteId;
        $this->apiKey = $apiKey;
        $this->debug = $debug;
    }

    public function call($method, $url, $data)
    {
        $session = curl_init();

        curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($session, CURLOPT_HTTPGET, 1);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_VERBOSE, 1);
        curl_setopt($session, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($session, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($session, CURLOPT_USERPWD, $this->siteId . ":" . $this->apiKey);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($session, CURLOPT_URL, $url);

        $response = curl_exec($session);


        $httpCode = curl_getinfo($session, CURLINFO_HTTP_CODE);

        if ($this->debug && $httpCode !== 200)
        {
            throw new HttpClientFailureException($httpCode . ': ' . $response);
        }


        curl_close($session);

        return $response;
    }
}