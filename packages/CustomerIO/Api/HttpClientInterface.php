<?php

namespace Packages\CustomerIO\Api;

interface HttpClientInterface
{
    public function call($method, $url, $data);
}