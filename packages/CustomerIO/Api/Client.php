<?php

namespace Packages\CustomerIO\Api;

class Client implements ClientInterface
{
    private
        $httpClient,
        $endpoints = [
        'api' => 'https://track.customer.io/api/v1'
    ];

    public function httpClient()
    {
        return $this->httpClient;
    }

    public function endpointUrl($index, $url)
    {
        return $this->endpoints[$index] . $url;
    }

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function customerEvent($id, $name, array $data = [])
    {
        $url = $this->endpointUrl('api', "/customers/{$id}/events");

        $params = [
            'name' => $name,
            'data' => $data
        ];

        return $this->httpClient->call('POST', $url, $params);
    }

    public function updateCustomer($id, $email, $createdAt, array $attributes = [])
    {
        $url = $this->endpointUrl('api', "/customers/{$id}");

        $params = [
            'email' => $email,
            'created_at' => $createdAt,
            'attributes' => $attributes
        ];

        return $this->httpClient->call('PUT', $url, $params);
    }
}