<?php

namespace Packages\CustomerIO\Api;

interface ClientInterface
{
    public function customerEvent($id, $name, array $data = []);

    public function updateCustomer($id, $email, $createdAt, array $attributes = []);
}