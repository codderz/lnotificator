<?php

namespace App\Http\Controllers\Notificator;

use App\Http\Controllers\Controller;
use App\Services\Notificator\Commands\AddEventCommand;
use App\Services\Notificator\Exceptions\NotificatorException;
use Illuminate\Http\Request;

class AddEventController extends Controller
{
    private
        $addEventService;

    public function __construct(AddEventCommand $addEventService)
    {
        $this->addEventService = $addEventService;
    }

    public function execute(Request $request)
    {
        try
        {
            $result = $this->addEventService->execute($request->all());

            return $this->responseSuccess($result);
        }
        catch (NotificatorException $exception)
        {
            return $this->responseError($exception->errors());
        }
    }

    private function responseSuccess($result)
    {
        return response()->json([
            'success' => true,
            'id' => $result['id']
        ]);
    }

    private function responseError($errors)
    {
        return response()->json([
            'error' => $errors
        ]);
    }
}
