<?php


namespace App\Services\Notificator\Transports\Sms;


use App\Event;
use App\Services\Notificator\Transports\TransportInterface;

class AnotherFakeSmsTransport implements TransportInterface
{
    public function send(Event $event)
    {
        //Another fake sms sending
    }
}