<?php


namespace App\Services\Notificator\Transports\Sms;


use App\Event;
use App\Services\Notificator\Transports\TransportInterface;

class FakeSmsTransport implements TransportInterface
{
    public function send(Event $event)
    {
        //Fake sms sending
    }
}