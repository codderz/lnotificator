<?php


namespace App\Services\Notificator\Transports;


use App\Event;

interface TransportInterface
{
    public function send(Event $event);
}