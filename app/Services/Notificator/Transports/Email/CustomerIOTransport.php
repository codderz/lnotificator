<?php


namespace App\Services\Notificator\Transports\Email;


use App\Event;
use App\Services\Notificator\Exceptions\TransportFailedException;
use App\Services\Notificator\Transports\TransportInterface;
use Packages\CustomerIO\Api\ClientInterface;

class CustomerIOTransport implements TransportInterface
{
    private
        $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function send(Event $event)
    {
        $customer = $event->customer;

        $customerId = $customer['id'];

        $customerEmail = $customer['email'];

        unset($customer['id'], $customer['email']);


        $this->client->updateCustomer($customerId, $customerEmail, $customer);

        $this->client->customerEvent($customerId, $event->name, $event->params);
    }
}