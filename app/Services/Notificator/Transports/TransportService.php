<?php

namespace App\Services\Notificator\Commands;

use App\Event;
use App\Services\Notificator\Transports\TransportInterface;
use Illuminate\Support\Facades\App;

class TransportService implements TransportInterface
{
    public function send(Event $event)
    {
        $channels = [];

        foreach ($event->channels as $channel)
        {
            try
            {
                $transport = config('notificator.transports' . $channel);

                if (!$transport) continue;

                App::make($transport)->send($event);
            }
            catch (\Exception $exception)
            {
                $channels[] = $channel;
            }
        }

        if ($channels)
        {
            $event->channels = $channels;

            $event->save();

            return;
        }

        $event->delete();

        //TODO: log to another table
    }
}