<?php

namespace App\Services\Notificator\Commands;


use App\Event;

class RunEventsCommand
{
    private
        $transportService;

    public function __construct(TransportService $transportService)
    {
        $this->transportService = $transportService;
    }

    public function execute()
    {
        $events = Event::orderBy('created_at')->get();

        foreach ($events as $event)
        {
            $this->transportService->send($event);
        }
    }

}