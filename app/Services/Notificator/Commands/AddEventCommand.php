<?php

namespace App\Services\Notificator\Commands;

use App\Event;
use App\EventSpec;
use App\Services\Notificator\Exceptions\NotificatorException;
use App\Services\SkyEng\Api\UserApiServiceInterface;
use Illuminate\Support\Facades\Validator;

class AddEventCommand
{
    private
        $customerApiService;

    public function __construct(UserApiServiceInterface $customerApiService)
    {
        $this->customerApiService = $customerApiService;
    }

    public function execute(array $request)
    {

        $validator = Validator::make($request, [
            'name' => 'required|max:255',
            'params' => 'array',
            'customerId' => 'required',
        ]);

        if ($validator->fails())
        {
            throw new NotificatorException($validator);
        }


        $name = $request['name'];

        $eventSpec = EventSpec::where('name', $name)->first();

        if (!$eventSpec)
        {
            $validator->errors()->add('event', 'Event not found');

            throw new NotificatorException($validator);
        }


        $params = $request['params'];

        if (array_diff($eventSpec->params, $params))
        {
            $validator->errors()->add('event-params', 'Parameters required');

            throw new NotificatorException($validator);
        }


        $customerId = $request['customerId'];

        $customer = $this->customerApiService->find($customerId);

        if (!$customer)
        {
            $validator->errors()->add('customer', 'Customer not found');

            throw new NotificatorException($validator);
        }

        $event = Event::makeFromSpec($eventSpec, $params, $customer);

        $event->save();

        return $event;
    }
}