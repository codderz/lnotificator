<?php

namespace App\Services\Notificator\Exceptions;


use Illuminate\Validation\Validator;
use Throwable;

class NotificatorException extends \Exception
{
    private
        $validator = [];

    public function __construct(Validator $validator, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->validator = $validator;
    }

    public function errors()
    {
        return $this->validator->errors();
    }
}