<?php

namespace App\Services\SkyEng\Api;

interface UserApiServiceInterface
{
    public function find($userId);
}