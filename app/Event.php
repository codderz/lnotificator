<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected
        $fillable = [
            'event_spec_id', 'customer', 'params', 'channels'
        ];

    public static function makeFromSpec(EventSpec $eventSpec, array $customer, array $params)
    {
        return new self([
            'event_spec_id' => $eventSpec->id,
            'channels' => $eventSpec->channels,
            'params' => $params,
            'customer' => $customer
        ]);
    }

    public function getParamsAttribute($value)
    {
        return json_decode($value);
    }

    public function setParamsAttribute(array $items = [])
    {
        $this->attributes['params'] = json_encode($items);
    }

    public function getCustomerAttribute($value)
    {
        return json_decode($value);
    }

    public function setCustomerAttribute(array $items = [])
    {
        $this->attributes['customer'] = json_encode($items);
    }

    public function getChannelsAttribute($value)
    {
        return json_decode($value);
    }

    public function setChannelsAttribute(array $items = [])
    {
        $this->attributes['channels'] = json_encode($items);
    }
}
