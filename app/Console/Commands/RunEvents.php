<?php

namespace App\Console\Commands;

use App\Services\Notificator\Commands\RunEventsCommand;
use Illuminate\Console\Command;

class RunEvents extends Command
{
    private
        $command;

    public function __construct(RunEventsCommand $command)
    {
        $this->command = $command;


        $this->signature = 'events:run';

        $this->description = 'Run all queued events';


        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->command->execute();
    }
}
