<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSpec extends Model
{
    public function getParamsAttribute($value)
    {
        return json_decode($value);
    }

    public function setParamsAttribute(array $items = [])
    {
        $this->attributes['params'] = json_encode($items);
    }

    public function getChannelsAttribute($value)
    {
        return json_decode($value);
    }

    public function setChannelsAttribute(array $items = [])
    {
        $this->attributes['channels'] = json_encode($items);
    }
}
